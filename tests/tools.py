from proteus import Model


def get_combined_location(config=None):
    Combined = Model.get('stock.location.combined')
    Location = Model.get('stock.location')

    combined = Combined.find([('name', '=', 'Combined 1')], limit=1)
    if combined:
        return combined[0]

    combined = Combined(name='Combined 1', code='C1')
    prod_loc, = Location.find([('type', '=', 'production'),
                               ('code', '=', 'PROD')], limit=1)

    for ii in range(1, 3):
        loc = Location(type='production')
        loc.name = 'Production %s' % ii
        loc.code = 'PL%s' % ii
        loc.parent = prod_loc
        loc.save()
        combined.locations.append(loc)

    combined.save()
    return combined
